const express = require("express");
const request = require("request");

const app = express();
let counter = 0;

app.get("/", (req, res) => {
  console.log("Received request number ", counter++);
  res.send("RCVD");
  setTimeout(
    () =>
      request.get(
        `http://${process.env.OTHER_HOST}:${process.env.OTHER_PORT}`,
        (e, r, b) => console.log("Received response: ", b)
      ),
    3000
  );
});

const server = app.listen(process.env.THIS_PORT, () => {
  console.log("Server 2 listening on port ", process.env.THIS_PORT);
  console.log(
    "The other server is: ",
    `http://${process.env.OTHER_HOST}:${process.env.OTHER_PORT}`
  );
});
